# Copyright 2016 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require systemd-service udev-rules

SUMMARY="Open Source Telephony"
DESCRIPTION="
oFono provides a mobile telephony (GSM/UMTS) application development framework that includes
consistent, minimal, and easy to use complete APIs. As an open source project, it includes a
high-level D-Bus API for use by telephony applications of any license. oFono also includes a
low-level plug-in API for integrating with open source as well as third party telephony stacks,
cellular modems, and storage back ends. The plug-in API functionality is modeled on public
standards, in particular 3GPP TS 27.007 \"AT command set for User Equipment (UE).\" oFono is sponsored
by Intel and most of the project maintainers work.
"
HOMEPAGE="https://www.${PN}.org/"
DOWNLOADS="mirror://kernel/linux/network/${PN}/${PNV}.tar.xz"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="bluetooth"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
    build+run:
        dev-libs/glib:2[>=2.32]
        net-misc/mobile-broadband-provider-info
        sys-apps/dbus[>=1.4]
        sys-apps/systemd
        sys-apps/upower
        bluetooth? ( net-wireless/bluez[>=5] )
"

BUGS_TO="keruspe@exherbo.org"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --localstatedir=/var
    --enable-datafiles
    --enable-debug
    --enable-dundee
    --enable-optimization
    --enable-pie
    --enable-provision
    --enable-test
    --enable-threads
    --enable-tools
    --enable-udev
    --enable-upower
    --disable-bluez4
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( bluetooth )

RESTRICT="test"

src_test() {
    esandbox allow_net unix:/tmp/unittestril*
    default
    esandbox disallow_net unix:/tmp/unittestril*
}

pkg_postinst() {
    default
    nonfatal edo udevadm trigger --subsystem-match=network --action=change
}

pkg_postrm() {
    default
    nonfatal edo udevadm trigger --subsystem-match=network --action=change
}

