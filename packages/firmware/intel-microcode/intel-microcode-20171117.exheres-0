# Copyright 2009 Jonathan Dahan <jedahan@gmail.com>
# Copyright 2012-2015 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based on 'microcode-data-20090330.ebuild', which is:
#     Copyright 1999-2009 Gentoo Foundation

require flag-o-matic

# https://downloadcenter.intel.com/download/27337
DOWNLOAD_ID=27337 #                         ^^^^^
MY_PNV=${PN/intel-/}-${PV/_p/-v}

SUMMARY="Intel microcode update data"
DESCRIPTION="
The microcode data file contains the latest microcode definitions for all Intel
processors. Intel releases microcode updates to correct processor behavior as
documented in the respective processor specification updates. While the regular
approach to getting this microcode update is via a BIOS upgrade, Intel realizes
that this can be an administrative hassle. The Linux Operating System and
VMware ESX products have a mechanism to update the microcode after booting. For
example, this file will be used by the operating system mechanism if the file
is placed in the /lib/firmware directory of the Linux system.
"
HOMEPAGE="https://downloadcenter.intel.com"
DOWNLOADS="https://downloadmirror.intel.com/${DOWNLOAD_ID}/eng/${MY_PNV}.tgz"

LICENCES="intel-ucode"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        sys-apps/iucode-tool
"

WORK=${WORKBASE}

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( releasenote )

src_prepare() {
    edo iucode_tool --write-earlyfw=microcode.cpio intel-ucode
}

src_install() {
    insinto /usr/$(exhost --target)/lib/firmware

    # install the large text microcode.dat (used by older kernels via microcode_ctl)
    doins microcode.dat

    # install a small initramfs for use with CONFIG_MICROCODE_EARLY
    doins microcode.cpio

    # install the split binary ucode files (used by the kernel directly)
    doins -r intel-ucode

    # and try to run the update on boot for systems not configured to use a initramfs
    insinto /usr/$(exhost --target)/lib/tmpfiles.d
    hereins ${PN}.conf <<EOF
w /sys/devices/system/cpu/microcode/reload - - - - 1
EOF

    emagicdocs
}

pkg_postinst() {
    elog "On Kernel >=3.9 the recommended and safest way to update the microcode is on early boot"
    elog "before running userspace by enabling the following kernel options:"
    elog ""
    elog "CONFIG_BLK_DEV_INITRD"
    elog "CONFIG_MICROCODE"
    elog "CONFIG_MICROCODE_EARLY"
    elog "CONFIG_MICROCODE_INTEL"
    elog ""
    elog "You can use /usr/$(exhost --target)/lib/firmware/microcode.cpio directly via the initrd"
    elog "kernel command line, add it to your existing initramfs, or via CONFIG_INITRAMFS_SOURCE."
    elog ""
    elog "The older, discouraged and dangerous way to is to not use an initramfs:"
    elog "The Intel CPU microcode has been updated and will be loaded by the kernel on"
    elog "the next boot if the CONFIG_MICROCODE_INTEL kernel option is enabled. If you're"
    elog "using the sys-apps/microcode_ctl baselayout-1 init daemon for management the"
    elog "microcode can be manually reloaded with:"
    elog "    eclectic rc restart microcode_ctl"
}

