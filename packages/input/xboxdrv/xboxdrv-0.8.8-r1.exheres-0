# Copyright 2014-2015 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ tag=v${PV} ] scons systemd-service

SUMMARY="Userspace Xbox/Xbox360 gamepad driver"
DESCRIPTION="
This is a Xbox/Xbox360 gamepad driver for Linux that works in userspace.
It is an alternative to the xpad kernel driver and has support for Xbox1
gamepads, Xbox360 USB gamepads and Xbox360 wireless gamepads, both first
and third party.

Unlike the stock xpad kernel driver, xboxdrv provides a wide varity of
configuration options, it allows you to simulate keyboard and mouse
events, remap buttons and axes, apply autofire, invert axis, tweak axis
sensitivity, emulate throttle and rudder controls and send macros.
"
HOMEPAGE+=" http://pingus.seul.org/~grumbel/${PN}"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        x11-proto/xproto
    build+run:
        dev-libs/boost
        dev-libs/dbus-glib:1
        dev-libs/glib:2
        sys-apps/dbus[>=1.9.18]
        virtual/usb:1
        x11-libs/libX11
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-Allow-to-specify-ranlib.patch
    "${FILES}"/${PN}-Fix-build-with-gcc6.patch
)

SCONS_SRC_CONFIGURE_PARAMS=( BUILD=custom AR=${AR} CC=${CC} CXX=${CXX} PKG_CONFIG=${PKG_CONFIG} RANLIB=${RANLIB} )
SCONS_SRC_COMPILE_PARAMS=( BUILD=custom AR=${AR} CC=${CC} CXX=${CXX} PKG_CONFIG=${PKG_CONFIG} RANLIB=${RANLIB} )

src_prepare() {
    default

    # Partially undo the gcc6 patch due to nullptr being undeclared for anything
    # *not* gcc6
    edo sed -i -e "s:nullptr:NULL:" src/controller_slot.cpp
}


src_install() {
    dobin xboxdrv
    doman doc/xboxdrv.1

    emagicdocs

    install_systemd_files

    insinto /usr/share/dbus-1/system.d
    doins data/org.seul.Xboxdrv.conf
}

